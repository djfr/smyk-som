'use strict';

/**
 * @ngdoc function
 * @name smykSomApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the smykSomApp
 */
angular.module('smykSomApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
