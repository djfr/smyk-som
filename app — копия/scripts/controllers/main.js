'use strict';

/**
 * @ngdoc function
 * @name smykSomApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the smykSomApp
 */
angular.module('smykSomApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
